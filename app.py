from flask import Flask, url_for, session, request
from flask import render_template, redirect
from authlib.integrations.flask_client import OAuth
from forms import StudyRegistrationForm

import gitlab

app = Flask(__name__)
# Load configuration from config file set in STYDYREG_SETTINGS environment variable.
app.config.from_envvar('STYDYREG_SETTINGS')

# TODO Use cache for storing token.
oauth = OAuth(app)
oauth.register(
    name = 'gitlab',
    access_token_url = 'https://data.ccns.sbg.ac.at/oauth/token',
    access_token_params=None,
    authorize_url = 'https://data.ccns.sbg.ac.at/oauth/authorize',
    authorize_params=None,
    client_kwargs={'scope': 'api'},
    api_base_url='https://data.ccns.sbg.ac.at/api/v4/',
)

@app.route('/')
def homepage():
    user = session.get('user')
    form = None
    if user:
        form = StudyRegistrationForm()
    return render_template('index.html', user=user, form=form)

@app.route('/submit', methods=['POST'])
def submit():
    form = request.form
    user = session.get('user')
    token = session.get('token')
    gl = gitlab.Gitlab(url='https://data.ccns.sbg.ac.at/', oauth_token=token['access_token'])

    # change ref to main before merging into main
    gitlab_ci_file = gl.projects.get(17).files.raw(file_path='.gitlab-ci.yml', ref='5-basic-gitlab-ci-yml-file')

    group_id = gl.groups.list(search=form['studynamespace'])[0].id
    project = gl.projects.create({'name': form['studyabbrev'], 'namespace_id': group_id})

    # See https://docs.gitlab.com/ce/api/commits.html#create-a-commit-with-multiple-files-and-actions
    # for actions detail
    data = {
        'branch': 'main',
        'commit_message': 'Create initial CI/CD configuration file.',
        'actions': [
            {
                'action': 'create',
                'file_path': '.gitlab-ci.yml',
                'content': gitlab_ci_file.decode(),
            }
        ]
    }

    commit = project.commits.create(data)
    return render_template('summary.html', form=form, user=user)

@app.route('/login')
def login():
    redirect_uri = url_for('authorize', _external=True)
    return oauth.gitlab.authorize_redirect(redirect_uri)

@app.route('/authorize')
def authorize():
    token = oauth.gitlab.authorize_access_token()
    session['token'] = token

    # Do something with the token.
    # Now, simply get user information from the GitLab API and store username in session.
    # https://docs.gitlab.com/ee/api/users.html#list-current-user-for-normal-users
    resp = oauth.gitlab.get('user', token=token)
    user = resp.json().get('username')
    if user:
        session['user'] = user
    return redirect('/')

@app.route('/logout')
def logout():
    session.pop('user')
    session.pop('token')
    return redirect('/')
