from flask_wtf import FlaskForm
from wtforms import StringField, SelectField
from wtforms.validators import InputRequired, Length

class StudyRegistrationForm(FlaskForm):
    studyabbrev = StringField('Study abbreviation',
            validators = [InputRequired(), Length(min=5, max=20)],
            description = "Type abbreviated name of your study",
            render_kw = {
                "class" : "input",
                "placeholder" : "Text input"
            }
    )
    studynamespace = StringField('Group',
            validators = [InputRequired(), Length(min=3, max=20)],
            description = "Namespace of the study data repository",
            render_kw = {
                "class" : "input",
                "placeholder" : "anc"
            }
    )
    dataowner = StringField('Data owner name',
            validators = [InputRequired(), Length(min=5, max=20)],
            description = "Name of the owner of study data repository",
            render_kw = {
                "class" : "input",
                "placeholder" : "Text input"
            }
    )
    site = SelectField('Acquisition site',
            choices = [('szg', 'Salzburg'), ('grz', 'Graz')],
            description = "Choose where the data will be acquired"
    )
