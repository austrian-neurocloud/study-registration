# Study Registration

A study registration survey that initiates data repositories.

## Deploy the app locally

### Prepare virtual environment

The dependencies are listed in [`requirements.txt`](requirements.txt).

Execute the following to prepare and activate virtual environment. On Debian install `python3-venv` package.

```
python3 -m venv venv
. venv/bin/activate
pip3 install -r requirements.txt
```

To deactivate the virtual environment, execute `deactivate` command.

### App settings

Save a settings file, for example `settings.cfg`, with the following content, where `XXX` has to be set.

```
# Set for signing cookies and sessions.
SECRET_KEY = 'XXX'

# OAuth app ID and app secret.
GITLAB_CLIENT_ID = 'XXX'
GITLAB_CLIENT_SECRET = 'XXX'
```

Set the `STYDYREG_SETTINGS` variable to point to the settings file.

```
export STYDYREG_SETTINGS=/path/to/settings.cfg
```

To [run Flask in development mode](https://flask.palletsprojects.com/en/2.1.x/quickstart/#debug-mode) with debugging enabled, `export FLASK_ENV=development`.

### Run the app

Execute `flask run`.

## User authentication

The user is authenticated using OAuth and [GitLab as OAuth provider](https://docs.gitlab.com/ee/api/oauth2.html). The authentication is implemented using Authlib and the following resources:
- https://docs.authlib.org/en/latest/client/frameworks.html
- https://docs.authlib.org/en/latest/client/flask.html
- https://github.com/authlib/demo-oauth-client/tree/master/flask-google-login

Study Registration app has to be [registered on GitLab](https://docs.gitlab.com/ee/integration/oauth_provider.html) to obtain OAuth variables set in [app settings](#app-settings). For development, set **Callback URL** to `http://127.0.0.1:5000/authorize`
